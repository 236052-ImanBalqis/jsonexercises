package json151122;

public class TheDetails {

		private String name;  //three fields or variables
	    private int age;
	    private String gender;
	    private String city;
	    private String country;
	    
		@Override
		public String toString() {
			return "TheDetails [name=" + name + ", age=" + age + ", gender=" + gender + ", city=" + city + ", country="
					+ country + "]";
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getCountry() {
			return country;
		}
		public void setCountry(String country) {
			this.country = country;
		}

		
	}


