package json151122;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;



public class TheJ2O {

	public static void main(String[] args) {

		Gson gson = new Gson();
		 
		 try(Reader read = new FileReader("C:\\Users\\Bil\\TheFile.json")){
			 TheDetails ed = gson.fromJson(read, TheDetails.class);
			 System.out.println(ed);
		 }
		 
		 catch (IOException e) {
			 e.printStackTrace();
		 }
	}

}
